This is a collection of scripts and snippets for generating a high-quality table of pairwise orthologous exons from any two species. It follows the steps described by [Blekhman et al 2010](http://www.ncbi.nlm.nih.gov/pubmed/20009012) and [Blekhman, 2012](http://precedings.nature.com/documents/7054/version/1), without any (intentional!) differences until the final steps of the pipeline - all differences are documented. The example used here is humans and crab eating macaques/cynomolgus monkey (*Macaca fascicularis*) but obviously there's no reason to limit oneself to that, and the pipeline can accept any two species provided one of them is well annotated and they both have relatively decent quality genomes. 

Throughout, I've chosen the more conservative approach whenever there was a choice. I'd rather be stringent and have fewer high quality exons at the end than sloppy. 

# Table of Contents #
* Reference files
    + Software needed
    + A note on the genome builds chosen
* Retrieving human exon FASTA sequences
* Splitting the fasta file into workable sizes
* Interspecies blat
* Interspecies processing
    + Filtering criteria
* Retrieving fasta sequence for second species
* Intraspecies blat
* Intraspecies processing
    + Filtering criteria
* Additional filtering steps
    + Orthology based filtering
    + Removing ribosome-associated genes
    + RNA-seq read simulation to identify mapping ambiguities
    + Further considerations
        + Orthology thresholds
* Contact


## Reference files:

* human genome: hg38 without alternate assemblies, retrieved from [ncbi](ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA_000001405.15_GRCh38/seqs_for_alignment_pipelines.ucsc_ids). 
* second genome: macFas5, retrieved from UCSC goldenpath ftp.
* exon database: ensembl86 (October 2016 release)

NCBI files have unwieldy chromosome names, and had to be renamed - thankfully this simply entailed chopping off all but the first element of each line:

`zcat GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.gz | awk '{print $1}' > hg38_renamed_no_alt_analysis_set.fa`

### Software needed

Apart from basic shell, I used the following:

* bedtools 2.25
* [BLAT v. 35x1](https://users.soe.ucsc.edu/~kent/src/)
* R 3.3.1
    + data.table 1.10.0
    + GenomicRanges 1.24.3
    + stringi 1.1.2
    + polyester 1.10.0
    + Biostrings 2.42.1
    + limma 3.28.21
    + edgeR 3.14.0
    + Rsubread []
    + and their associated dependencies

### A note on the genome builds chosen: 

Because there is no actual filtering for genome-wide mappability in this pipeline (but see the RNA-seq filtering section below), I've retained all of the unplaced and random contigs in both species throughout the pipeline. I could have stuck to only canonical and assembled chromosomes, but that would have artifically led to keeping regions that might not actually be the only possible source of RNA seq reads (for example, lots of ribosomal RNA sequences are in the unplaced contigs). The genomes used are soft-masked for similar reasons.

When using the final file, it's straightforward to discard anything outside your favourite chromosomes.

## Retrieving human exon FASTA sequences:

This can definitely be done through an API and/or biomaRt, but I actually like using the bioMart website, because it lets me make sure I'm doing everything right visually. It's slow and pokey, but that's fine, sometimes. And anyhow, the time commitment to learning to use the API was not worth it. 

In any case, I retrieved the sequence (plus gene ID, transcript ID, exon ID, strand, chr, start, end) of every single exon in the human genome as of ensembl84. I also retrieved the coordinates as a bed file, for checking downstream that things align where they should:

Overall number of entries is 737,982 unique exons across 63,305 Ensembl genes. This is rather different from the numbers reported by Blekhman 2012; annotations have clearly improved since then (we hope). 

## Splitting the fasta file into workable sizes

Used `fasta_splitter.pl`, which I found on the internet and modified minorly to make more interactive. Usage is straightforward - two arguments need to be specified at the command line, the name of the fasta file and the number of records per split file: 

    fasta_splitter.pl ensembl_v86_all_sequences_170124.fa 10000

## Interspecies blat

First step in the pipeline is to blat every single human exon against the second species' genome. I ran blat with all default options except for -noHead; they seemed sensible and conservative. Because NSCC has decided to build a custom wrapper for job submission (wtf?), echoing to qsub and trying to run things as an array are not compatible. So I just launch one job for each split fasta file, and the queueing system be damned. 

    for i in {1..74}; do
        echo "blat -noHead ~/orthoexon/macFas5/macFas5.fa ~/orthoexon/exon_hg38v86/ensembl_v86_all_sequences_170124.fa${i}.fa ~/orthoexon/exon_hg38v86/blat_output/hg38_to_macFas5/macFas5_${i}_blat.out" | qsub -l ncpus=1,walltime=4:00:00 -V -o ~/logs/blat_hg_macFas_$i.log -N blat_hg_macFas_$i -M igr@ntu.edu.sg -m ae -oe
    done

4 hours is actually a ridiculous length of time for the vast majority of blat jobs, but a couple of them were taking more than the 1h I was originally allocating them, so...

## Interspecies processing

First thing to do is to concatenate all the blat output files into a single file for reading into R.

`for i in {1..74}; do cat macFas5_${i}_blat.out >> macFas5_blat_all_hg38_exons.out; done`

(n.b. given my directory structure it's ok for this to be the same file name as that generated in the intraspecies processing)

After that, the file can be processed by R using `blat_processing_interspecies.R`. This can be run through Rscript or similar, without any command line arguments; everything is hardcoded but easy to modify. It produces a bed file of all exons that pass the filtering criteria, `macFas_0.90_orthoexon_filter.bed `, which is used in the next step. 

### Filtering criteria

From the paper, thresholds for this step:

*"To find the non-human primate orthologous sequences for the human exons, we downloaded the full genome sequences of chimpanzee (Pan troglodytes, March 2006 draft, panTro2) and rhesus macaque (Macaca mulatta, January 2006 draft, rheMac2) from the UCSC Genome Browser database (www.genome.ucsc.edu). We used Blat (Kent 2002) to find for each human exon the putative corresponding positions in the chimpanzee (panTro2) and rhesus macaque (rheMac2) genomes. We excluded exons for which the best hit had either lower than 96% identity in chimpanzee or lower than 92% identity in rhesus macaque (percent identity was defined as the proportion of bases in the query human exons for which a perfect match was found in the non-human primate target exon; see below for a discussion on the choice of cutoffs). In addition, alignments with indels longer than 25bp were excluded. This step resulted in the identification of 222,287 orthologous exons (in 28,299 genes) in chimpanzee, and 193,632 orthologous exons (in 24,598 genes) in rhesus macaque."* 

Because the percent identity filters are set dynamically after looking at data, which I have not done at this point (see the supplement to Blekhman 2010), I retained all exons with a percent identity >= 90% instead, so there's no need to redo this step and be filled with regret later if 92% turns out to not be ideal. The filtering on the basis of interspecies orthology values is done later on, in `blat_processing_intraspecies.R`. 

After this round of filtering, there are 588,689 exons across [xxx] ENSEMBL genes.

## Retrieve fasta exon sequence for second species

To clean up `macFas_0.90_orthoexon_filter.bed `, simply remove the transcript IDs and keep the gene and exon IDs, which are sufficient for downstream merging purposes (since we can't really look at isoform data, and anyhow, it makes processing things in R a lot easier):

`sed 's/ENST[0-9]*;\{0,\}//g' macFas_0.90_orthoexon_filter.bed > macFas_0.90_orthoexon_filter_clean.bed`

After that, the only necessary thing is bedtools:

`bedtools getfasta -fi ~/orthoexon/macFas5/macFas5.fa -bed macFas_0.90_orthoexon_filter_clean.bed -name -fo ../macFas5_to_macFas5/macFas_0.90_orthoexon_filter.fa`

## Intraspecies blat

Next up we need to blat each species against itself, as per the paper. Here again I could've filtered the human exons to cut down on the number of blat jobs, but there was no real need for it - they'll all get filtered out when I merge across species by exon anyhow. So this is very similar to the interspecies blat step above, but with slight modifications to the command line, and also a different number of jobs per species (way more human exons). The first step is to split the fasta file generated in the previous step into a sensible number of files, and again I used `fasta_splitter.pl` with the same setting of 10000 records per line. Once that's done, the command lines are

Macaque:

    for i in {1..59}; do
        echo "blat -noHead ~/orthoexon/macFas5/macFas5.fa /home/users/ntu/igr/orthoexon/exon_hg38v86/blat_output/macFas5_to_macFas5/macFas_0.90_orthoexon_filter.fa${i}.fa ~/orthoexon/exon_hg38v86/blat_output/macFas5_to_macFas5/macFas5_${i}_blat.out" | qsub -l ncpus=2,walltime=24:00:00 -V -o ~/logs/blat_macFas_autos_$i.log -N blat_macFas_auto_$i -M igr@ntu.edu.sg -m ae -oe
    done

Human:

    for i in {1..74}; do
       echo "blat -noHead ~/orthoexon/hg38/hg38_renamed_no_alt_analysis_set.fa ~/orthoexon/exon_hg38v86/ensembl_v86_all_sequences_170124.fa${i}.fa ~/orthoexon/exon_hg38v86/blat_output/hg38_to_hg38/hg38_${i}_blat.out" | qsub -l ncpus=2,walltime=24:00:00 -V -o ~/logs/blat_hg_hg_$i.log -N blat_hg_hg_$i -M igr@ntu.edu.sg -m ae -oe
    done

Output is concatenated from the relevant directory with

    for i in {1..74}; do cat hg38_${i}_blat.out >> hg38_blat_all_hg38_exons.out; done 
    for i in {1..59}; do cat macFas5_${i}_blat.out >> macFas5_blat_all_hg38_exons.out; done 

## Intraspecies processing

Similar to above, the script used in this step is `blat_processing_intraspecies.R`. As it stands right now it can be run through Rscript, but there's a few variables that are hardcoded and could be turned into command line arguments if one is going to use the pipeline for many different species. I like running things using source and setting echo=T, because I like watching the script progress through every step. ¯\\\_(ツ)\_/¯

### Filtering criteria
Once more, from the paper:

*"We then excluded exons that might be positioned within repetitive regions, in any of the three genomes, as this could lead to ambiguous mapping of reads (and thus to spurious estimates of gene expression differences between species). To do so, we used Blat to map all of a species’ exons against its own genome. We excluded from further analyses any exon for which (i) the best hit was not the original exon position, or (ii) the second-best hit had higher than 90% identity. Of the remaining orthologous exons, 163,487 were shared across the three species. Finally, we identified all cases of overlapping exons (Ensembl annotations include overlapping exons), excluded exons which were overlapping in one or two, but not in all three species, and retained and combined overlapping exons across all three species. This final step resulted in the definition of 150,107 meta-exons (in 20,689 Ensembl genes) with orthologs in human, chimpanzee, and rhesus macaque."*

The overlapping exon bit is fairly straightforward with two species and can be done easily by using the `reduce` command from the GenomicRanges package. The only tricky bit is throwing out regions where different exons overlap in the two species, but again, that is now done and documented in the code. 

Additional criteria I introduced were

1. excluding overlapping exons that were associated with more than one unique ensembl gene ID. There were not many (~1200/species, the number can be easily calculated looking at the code)
2. excluding all exons associated with any gene annotated to two or more chromosomes in either species. 
3. excluding all exons where the *difference in intron size between the two species* is >= 10,000 bp, suggestive of poor blatting or annotation (this number is arbitrary, but roughly 0.5% of exons fail to meet this threshold)

At varying percent identity thresholds the final number of retained exons between human (hg38, ensembl v86) and crab eating macaque (macFas5) is:

 % identity | Unique genes | Unique exons
------------|--------------|--------------
**0.9**     | 36919        | 208712
**0.91**    | 35821        | 204417 
**0.92**    | 34142        | 198172
**0.93**    | 31787        | 188834
**0.94**    | 28850        | 176470
**0.95**    | 25461        | 160770

Note that **this file is strand-aware**. Transcripts that physically overlap but are on different strands are **not** treated as overlapping and are retained - you can filter them out using the GenomicRanges command `reduce`, turning off strand awareness, and then deleting any interval that's associated with both strands.

## Additional filtering steps:

`blat_processing_intraspecies.R` writes both a meta file as well as species-specific tab delimited SAF files (pseudo bedfiles, really) that can be used directy with featureCounts or similar. 

* hg38-macFas5 (**ensembl v84** [these are old and out of date]) files are [here](https://bitbucket.org/ee_reh_neh/orthoexon/src/a2a5b4b0d42b5499e4297b7e2b9f677d0296fba8/final_files/hg38_macFas5_ortho/?at=master)
* hg38-panTro3[1] (**ensembl v84** [these are old and out of date]) files are [here](https://bitbucket.org/ee_reh_neh/orthoexon/src/a2a5b4b0d42b5499e4297b7e2b9f677d0296fba8/final_files/hg38_panTro3_ortho/?at=master) 

Please note that **these files are currently out of date.** If you want ensembl v86 files, drop me a note. 

[1] *Sensu* NCBI. In all its 45k contigs glory. 

While the file produced at the end of this step is usable and matches the file produced at the end of the Blekhman et al 2008, 2010 papers, it is possible to filter it further if one desires.

### Orthology based filtering:

Beyond removing hits to non-canonical chromosomes, an idea from [Pfefferle and Wray 2013](http://gbe.oxfordjournals.org/cgi/doi/10.1093/gbe/evt148) is to filter on the basis of Ensembl orthology annotations (see [here for more info](http://www.ensembl.org/Help/View?id=135)), and retain only those genes with known 1-1 orthologuous genes.

This step is implemented in `blat_processing_interspecies.R` and controlled with the ortho_TRUE/ortho_FALSE flag. In my experience, one ends up throwing out a lot of perfectly sensible genes when doing this, because around half of the genes retained are not annotated as anything. Additionally, there's currently no ensembl annotations for *M. fascicularis*, so you have to do with *M. mulatta*, which is far from ideal. But if you really want to be stringent, it's worth considering. 

### Removing ribosome-associated genes:

The other big problem with using the file as it stands is that for some reason a lot of ribosomal genes pass all the threshold steps but cannot be mapped to with equal confidence in both species (I saw this a lot in our [chimp iPSC paper](https://elifesciences.org/content/4/e07103), although it never made it to the published version; early analyses suggested a concerted switch in ribosomal protein repertoire between the two species, which seemed, to put it charitably, highly improbable). It's actually really hard to flag all the genes that match these criteria, but on the whole the approach is similar to that mentioned by [Pfefferle and Wray 2013](http://gbe.oxfordjournals.org/cgi/doi/10.1093/gbe/evt148) (again). 

This step is currently **not implemented** in the pipeline, but can be easily added: here is some example code to get the right set of genes from downstream analyses I've been working on:

    ensembl <- useMart("ENSEMBL_MART_ENSEMBL", dataset="hsapiens_gene_ensembl", host="oct2016.archive.ensembl.org") # Ensembl 86, or your favourite version.
    geneFamilies <- getBM(attributes=c("ensembl_gene_id", "family", "family_description", "external_gene_name"), mart=ensembl)
    badRibosomes <- geneFamilies[grepl("MRP|RPS|RPL|RP L|RP S", geneFamilies$family_description),1] # 
    moreBadRibos <- geneFamilies[grepl("MRP|RPS|RPL|RP L|RP S", geneFamilies$external_gene_name),1]
    allBadRibos <- c(badRibosomes, moreBadRibos)

The number of lost genes here is very small (100-200, depending on your starting file), but it makes very big difference to top GO terms and DE genes. There are still stragglers around, I've not yet found the right combination of terms to filter out, but in my experience those few terms tend to catch the vast majority of problem genes. Ensembl annotations on gene names are pretty hard to parse, though. 


### RNA-seq read simulation to identify mapping ambiguities:

I've often noticed that pseudogenes and retrotransposition events make it through all the filters so far but result in artifactual DE results when working with real data - blatting examines the entire exon without taking into account read lenght; the entire exon might be unique but your (shorter) kmers not so. To identify exons that can be a problem in this situation, I have adopted part of the approach proposed by [by Zhu et al](http://bmcgenomics.biomedcentral.com/articles/10.1186/1471-2164-15-343) to simulate RNA-seq reads from the final file under no differential expression, test for differential expression (limma-voom) and discard meta-exons that come up as DE anyhow, since they are suggestive of mapping problems. 

There are problems here in choosing what to treat as a transcript and what not to. We are not using comprehensive transcripts but rather a subset of meta exons, some of which have no bearing on real biology, and treating all of them as constitutive - reads are simulated from an ordered concatenation of all meta exons associated with a single gene. Better ideas are welcome. 

Regardless, this step first requires generating said concatenations by writing a bed12 file which is then passed on to bedtools. This can be done with `blat_processing_intraspecies_bed12.R`, which is identical to `blat_processing_intraspecies.R` in all aspects except for the output file format. Once this file has been created, fasta files to start polyester simulations can be generated by using the `bedtools getfasta` command as above. Note that given R's penchant for hardcoded line breaks once a line goes past 500 characters we need to be a bit cunning, hence the sed commands:

    for i in 0.9 0.91 0.92 0.93 0.94 0.95; do
        for j in FALSE; do
            sed "s/\n//g" ~/orthoexon/simNGS/hg38v86_macFas5/hg38_ensembl86_orthoexon_${i}pc_ortho_${j}.bed12 | sed "s/chr/\nchr/g" | bedtools getfasta -fi ~/orthoexon/hg38/hg38_renamed_no_alt_analysis_set.fa -bed stdin -name -s -split -fo ~/orthoexon/simNGS/hg38v86_macFas5/fasta/hg38_ensembl86_orthoexon_${i}pc_ortho_${j}.fa

            sed "s/\n//g" ~/orthoexon/simNGS/hg38v86_macFas5/macFas5_ensembl86_orthoexon_${i}pc_ortho_${j}.bed12 | sed "s/chr/\nchr/g" | bedtools getfasta -fi ~/orthoexon/macFas5/macFas5.fa -bed stdin -name -s  -split -fo ~/orthoexon/simNGS/hg38v86_macFas5/fasta/macFas5_ensembl86_orthoexon_${i}pc_ortho_${j}.fa
        done
    done

Once *that* is done, we can run polyester through `sim_RNA_seq.r` to simulate RNA seq reads. The snippet below simulates both 50 and 100 bp reads, but obviously use whatever you prefer. Seed is hardcoded for now, but does not need to be. 

    for i in ortho_FALSE; do
        for j in 0.96 0.97 0.98 0.99 0.95; do
            for k in 50 100; do
                for l in panTro5; do
                    for m in 10 20 100; do
                        echo "module load R; Rscript --vanilla ~/repos/orthoexon/sim_RNA_seq.r ${i} ${j} ${k} ${l} ${m}" | qsub -V -j oe -l walltime=10:00:00 -l select=1:mem=20gb:ncpus=6 -N ${i}_${j}_${k}_${l}_${m}_poly -o ~/logs/polyester_${i}_${j}_${k}_${l}_${m}.log -M igr@ntu.edu.sg -m ae;
                    done;
                done;
            done;
        done;
    done

The current set up of the script simulates 10 sets of reads at three different depths that correspond to ~5, 10 and 50 million reads per individual. All transcripts are simulated to the same depth - 10x, 20x and 100x, `${m}` - across both species, with no differential expression, from a concatenation of the orthologous metaexons. It is worth thinking about adjusting this parameter to get something that looks a bit more real - either by using real biological data, or by drawing these target RPKMs from some sort of distribution, but I've chosen to simulate unrealistic situations instead so I can capture mappability failibility at different depths and not be confounded by biology. 

To keep things straightforward, I map and quantify using Rsubread 1.18.0 (and featureCounts) and don't keep the fasta files around once I'm done; see . Genomes need to be indexed with subread before starting, which is done by `subread_index.r`. 

`run-subread.sh` will launch subread in tiny arrays. Could have probably incorporated the featureCounts step into it, but that one is instead controlled by `extract_coverage_and_featurecounts.sh`. Once that's finished we map by launching `simulation_DE_testing.r`, which is closely modelled on other interspecies DE scripts I've written before; it outputs a bunch of qc graphs and DE results at the exon level for each run of the data

Right now the filtering threshold for keeping exons is log2(cpm) >= 0 in half the samples from one species (although in principle it will be all samples from that species, I would expect, since there's no biological signal in here), which is more permissive than my threshold of 2 (genic) per species - this is invariant regardless of depth. Worth reconsidering making it more flexible, since we're looking at exons not at genes (should we threshold at all?). OTOH, lowering it too far messes with the FDR testing. Hence we repeat it at the gene level and compare the two sets of results too. 



Regardless, once that has run we can finally analyse the output and get a list of falsely DE exons... which we can then finally remove! For that, we use `process_simulated_de.r`.




### Further considerations:

It's still worth thinking about mappability and how to assess it at a deeper level than simply exonic, because I think the solution here can be improved. The approach we've used before for genomic data sets - calculating mappability for every kmer, windowing and filtering on windows with an excess of non-unique kmers fails the moment splicing is brought into play, because you'd have to calculate mappability for all possible isoforms, which will never happen. Maybe as reads get longer this problem will solve itself, but I'm not fully convinced.

#### Orthology thresholds:

One more thing worth considering in greater depth is the issue of the soft vs hard orthology thresholds. The pipeline generates files at multiple thresholds, but ideally one wouldn't have to run analyses across all possible versions and choose post-hoc. In the Blekhman paper the rationale is to go for the most stringent threshold where the number of up and down regulated DE genes (without hardcore normalisation (eg limma) because those methods make that assumption in the first place and then go from there - another pickle) is roughly equal between the two species being compared. Working with these new files and macFas data I did confirm that 92% threshold was good, but it was not much better than 93 or 94%, and the results were not quantitatively different. That said, thisobservation is made a bit more complex by the fact that the data I used to test these files comes from differentiated stem cells that were not purified pre-sequencing, where that expectation of equal numbers of DE up and down might not *necessarily* be met. 

I think it's worth considering framing this as a much more rudimentary linear regression problem and going from there, but that's pending. 

## Contact
Send any and all suggestions, comments or bugs to Irene Gallego Romero (igr at ntu dot edu dot sg)