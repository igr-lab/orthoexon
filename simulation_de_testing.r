### simulation_de_testing.R
### Modelled closely on main_analysis_final.R from IGR's chimp-human iPSC paper
### IGR 2017.02.09

### 0. PREPARE WORKSPACE. ###
### 1. READ IN COUNT MATRICES AND EXON LISTS. ### 
### 2. CALCULATE NORMALISED LIBRARY SIZES, COMPUTE CPM, FILTER, LOESS NORMALISE, AND COMPUTE RPKM. ###
### 3. REPRODUCIBILITY, RAREFACTION AND PCA PLOTS ###
### 4. DIFFERENTIAL EXPRESSION TESTING. ###

# for i in ortho_FALSE; do
#     for j in 0.99 0.98 0.97 0.96 0.95; do
#         for k in 50 100; do
#             for l in 10 20 100; do
#                 for m in panTro5; do

#                 echo "module load R; Rscript --vanilla ~/repos/orthoexon/simulation_de_testing.r ${m} ${i} ${j} ${k} ${l}" | qsub -l walltime=1:00:00 -l ncpus=1 -V -o ~/orthoexon/simNGS/hg38v86_${m}/logs/poly_de_testing_${i}_${j}_${k}_${l}.log -N simDE${i}.${j}.${k}.${l} 

                  # done
#             done
#         done
#     done
# done

#############################
### 0. PREPARE WORKSPACE. ###
#############################

extraVars <- commandArgs(trailingOnly=T)
# extraVars <- c("panTro5", "ortho_FALSE", 0.97, 50, 100) 
# extraVars <- c("macFas5", "ortho_FALSE", 0.92, 100, 100) 

### load libraries
library(gplots)
library(RColorBrewer)
library(limma)
library(edgeR)
library(plyr)
library(VennDiagram)
library(ggplot2)
library(grid)
library(gridBase)

options(width=200)
setwd(paste0("/home/users/ntu/igr/orthoexon/simNGS/hg38v86_", extraVars[1], "/rpkm"))

pal <- brewer.pal(3, "Set2")

#################################################
### 1. READ IN COUNT MATRICES AND EXON LISTS. ### 
#################################################

hsOrthoCounts <- read.table(paste0("human_", extraVars[3], "pc_", extraVars[2], "_", extraVars[4], "bp_", extraVars[5], "x_clean.out"), header=T)
ssOrthoCounts <- read.table(paste0(extraVars[1], "_", extraVars[3], "pc_", extraVars[2], "_", extraVars[4], "bp_", extraVars[5], "x_clean.out"), header=T)

hsExonId <- by(hsOrthoCounts, hsOrthoCounts$Geneid, function(x) paste(x$Geneid, seq_along(x$Geneid), sep="_")) # SHOULD be identical, but you never know...
ssExonId <- by(ssOrthoCounts, ssOrthoCounts$Geneid, function(x) paste(x$Geneid, seq_along(x$Geneid), sep="_")) 

hsOrthoCounts$Exonid <- unlist(hsExonId)
ssOrthoCounts$Exonid <- unlist(ssExonId)

mergedCounts <- merge(hsOrthoCounts[,c(17,7:16)], ssOrthoCounts[,c(17,7:16)], by.x="Exonid", by.y="Exonid", all=T, suffix=c(".hs", ".ss"))
dim(mergedCounts)

mergedCounts$hsLength <- hsOrthoCounts[,6]
mergedCounts$ssLength <- ssOrthoCounts[,6]


#prepare meta information from sample names:
countsNames <- colnames(mergedCounts[,2:21])
speciesCol <- c(rep("darkorchid4", 10), rep("orange", 10))
samplesMeta <- data.frame(countsNames, speciesCol)
names(samplesMeta) <- c("line", "col")
samplesMeta$cex <- 1.5

    #alternative colours etc:
    samplesMeta$species <- ifelse(grepl("ss$", samplesMeta$line), extraVars[1], "Hs")
    samplesMeta$pch <- ifelse(grepl("ss$", samplesMeta$line), 16, 17)


######################################################################################################
### 2. CALCULATE NORMALISED LIBRARY SIZES, COMPUTE CPM, FILTER, LOESS NORMALISE, AND COMPUTE RPKM. ###
######################################################################################################

# reads into edgeR, calculate TMM and then CPM, writing out the intermediate steps:
mergedCountsDge <- DGEList(counts=as.matrix(mergedCounts[,2:21]), genes=mergedCounts[,1])
mergedCountsDge <- calcNormFactors(mergedCountsDge)

### Calculate log2 CPM
cpmNorm <- cpm(mergedCountsDge, normalized.lib.sizes=TRUE, log=TRUE, prior.count=0.25) 
pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_cpm.density_clean.pdf"))
plotDensities(cpmNorm, group=samplesMeta$species) 
abline(v=1)
dev.off()

# Filter on observing cpm greater or equal to 1 or more in at least half of the individuals in one species, not keeping library sizes.
mergedCountsDgeFilt <- mergedCountsDge[rowSums(cpmNorm[,1:10] >= 0) >= 5 | rowSums(cpmNorm[,11:20] >= 0) >= 5 , , keep.lib.sizes=F] 
print(paste0("Number of retained exons after CPM filtering"))
print(dim(mergedCountsDgeFilt))

pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_cpmFilt.density_clean.pdf"))
plotDensities(mergedCountsDgeFilt, group=samplesMeta$species)
dev.off()

# Recalculate TMM and lib sizes
mergedCountsDgeFilt <- calcNormFactors(mergedCountsDgeFilt) ## recalculate norm factors
# save(mergedCountsDgeFilt, file="mergedCountsDge.TMMFilt_clean.RDa")

# Loess normalise the data
## For starters, Voom requires a design matrix as input
design <- model.matrix(~ 0 + samplesMeta$species)
colnames(design) <- c("human", extraVars[1])

## Voom on filtered nonGC normalized data, with cyclic loess normalization, and blocked by individual replicates - this requires two passes, one without the random individual effect and a second one that takes it into account - see the limma manual and this post and reply by Gordon Smyth!
## https://support.bioconductor.org/p/59700/

## note that cyclic loess is designed for between-array normalisation rather than RNA-seq, but it should still be usable. Ideally we would not need it, however, but I don't like the directionality of the results without normalisation, not to mention there's a clear outlier sample in there. 
cpmNormLoess <- voom(mergedCountsDgeFilt, design, normalize.method="cyclicloess", plot=F) 
cpmNormNone <- voom(mergedCountsDgeFilt, design, normalize.method="none", plot=F) 
cpmNorm <- voom(mergedCountsDge, design, normalize.method="none", plot=F) 

## Turn this into RPKM
# Values are already log2 CPM, so just need to substract
rpkmNormLoess <- cpmNormLoess
rpkmNormLoess$E[,1:10] <- cpmNormLoess$E[,1:10] - log2((mergedCounts[rowSums(cpmNorm$E[,1:10] > 0) >= 5 | rowSums(cpmNorm$E[,11:20] > 0) >= 5 , 22])/1000)
rpkmNormLoess$E[,11:20] <- cpmNormLoess$E[,11:20] - log2((mergedCounts[rowSums(cpmNorm$E[,1:10] > 0) >= 5 | rowSums(cpmNorm$E[,11:20] > 0) >= 5 , 23])/1000)

rpkmNormNone <- cpmNormNone
rpkmNormNone$E[,1:10] <- cpmNormNone$E[,1:10] - log2((mergedCounts[rowSums(cpmNorm$E[,1:10] > 0) >= 5 | rowSums(cpmNorm$E[,11:20] > 0) >= 5 , 22])/1000)
rpkmNormNone$E[,11:20] <- cpmNormNone$E[,11:20] - log2((mergedCounts[rowSums(cpmNorm$E[,1:10] > 0) >= 5 | rowSums(cpmNorm$E[,11:20] > 0) >= 5 , 23])/1000)

rpkmNorm <- cpmNorm
rpkmNorm$E[,1:10] <- cpmNorm$E[,1:10] - log2((mergedCounts[, 22])/1000)
rpkmNorm$E[,11:20] <- cpmNorm$E[,11:20] - log2((mergedCounts[, 23])/1000)

pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_voom.all_norm.rpkm.indrandom.density_clean.pdf"))
plotDensities(rpkmNormLoess, group=samplesMeta$species) #again, a beautiful distribution, as expected. 
plotDensities(rpkmNormNone, group=samplesMeta$species) #again, a beautiful distribution, as expected. 
plotDensities(rpkmNorm, group=samplesMeta$species) #again, a beautiful distribution, as expected. 
dev.off()

#####################################################
### 3. REPRODUCIBILITY, RAREFACTION AND PCA PLOTS ###
#####################################################

# some barplots about mapping stats
# "Raw" library sizes:
pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_mapped_reads_raw_clean.pdf"))
mp <- barplot(sort(colSums(mergedCountsDge$counts)), ylab="Number of reads mapped to orthologous exons", xlab="", col="darkgrey", xaxt="n")
text(mp, -200000, srt = 45, adj = 1, labels = names(sort(colSums(mergedCountsDge$counts))), xpd = TRUE, cex=0.8)
dev.off()

# normalised library sizes
pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_mapped_reads_normalised_clean.pdf"))
mp <- barplot(sort(mergedCountsDge$samples$lib.size * mergedCountsDge$samples$norm.factor), ylab="Normalized library sizes", xlab="", xaxt="n", col="darkgrey")
text(mp, -200000, srt = 45, adj = 1, labels = row.names(mergedCountsDge$samples[order(mergedCountsDge$samples$lib.size * mergedCountsDge$samples$norm.factor), ]), xpd = TRUE, cex=0.8)
dev.off()

# number of genes expressed
pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_number_of_genes_expressed_clean.pdf"))
some.counts <- apply(mergedCountsDge$counts, 2, function(x) { sum(x > 0) })
mp <- barplot(sort(some.counts), ylab="Number of genes with at least 1 read", xlab="", xaxt="n", col="darkgrey")
text(mp, -500, srt = 45, adj = 1, labels = names(sort(some.counts)), xpd = TRUE, cex=0.8)
dev.off()

# Perform rarefaction curves for number of expressed genes vs. proportion of pool mRNA
# As in Ramskold D, Wang ET, Burge CB, Sandberg R. 2009. An abundance of ubiquitously expressed genes revealed by tissue transcriptome sequence data. PLoS Comput Biol 5:e1000598.
# This gives an idea of the complexity of transcriptome in different tissues

# # using ortho Exons genes aggregates
# pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_rarefaction_curves_clean.pdf"))
# plot(1:length(mergedCountsDge$counts[,1]), cumsum(sort(mergedCountsDge$counts[,1], decreasing=T)/sum(mergedCountsDge$counts[,1])), log="x", type="n", xlab="Number of genes", ylab="Fraction of reads pool", ylim=c(0,1)) ## initialize the plot area
# apply(mergedCountsDge)
#   lines(1:lengt
#     h(mergedCountsDge$counts[,simSample]), cumsum(sort(mergedCountsDge$counts[,simSample], decreasing=T)/sum(mergedCountsDge$counts[,simSample])), col=samplesMeta$col], lwd=2)
#   # lines(1:length(mergedCountsDge$counts[,sample]), cumsum(sort(mergedCountsDge$counts[,sample], decreasing=T)/sum(mergedCountsDge$counts[,sample])), col=as.character(samplesMeta[samplesMeta$line %in% sample,]$col), lwd=2)
# }
# legend(x="topleft", bty="n", col=c("darkorchid4", "orange"), legend=c("human", extraVars[1]), lty=1, lwd=2)
# dev.off()

# PCA plotting function:
plot.pca <- function(dataToPca, speciesCol, namesPch, sampleNames){
    # check for invariant rows:
    dataToPca.clean <- dataToPca[!apply(dataToPca, 1, var) == 0,]
    pca <- prcomp(t(dataToPca.clean), scale=T, center=T)
    pca.var <- pca$sdev^2/sum(pca$sdev^2)
    plot(pca$x[,1], pca$x[,2], col=speciesCol, pch=namesPch, cex=2, xlab=paste("PC1 (", round(pca.var[1]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""))
    # legend(col=speciesCol, legend=sampleNames, pch=namesPch, x="bottomright", cex=0.6)
    plot(pca$x[,2], pca$x[,3], col=speciesCol, pch=namesPch, cex=2, xlab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""))
    # legend(col=speciesCol, legend=sampleNames, pch=namesPch, x="topright", cex=0.6)
    plot(pca$x[,3], pca$x[,4], col=speciesCol, pch=namesPch, cex=2, xlab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC4 (", round(pca.var[4]*100, digits=2), "% of variance)", sep=""))
    # legend(col=speciesCol, legend=sampleNames, pch=namesPch, x="topright", cex=0.6)

    return(pca)

}

# Actually plotting the PCA
pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_voom.loess.cpm.indrandom.pca_clean.pdf"))
pcaresults <- plot.pca(rpkmNormLoess$E, samplesMeta$col, samplesMeta$pch, samplesMeta$line)
dev.off()

# Reproducibility (pretty dull with only one variable, but still)
plot.reproducibility <- function(data.to.test, metadata, method){
    cor.mat <- cor(data.to.test, method=method, use="pairwise.complete.obs")

    species.rep <- vector()
    between.species <- vector()

    for (i in 1:ncol(data.to.test)){
        for (j in 1:ncol(data.to.test)){
            if (j > i){
                if (metadata$species[i] == metadata$species[j]){
                    species.rep <- c(species.rep, cor.mat[i,j])
                } else {between.species <- c(between.species, cor.mat[i,j])}
            }
        }
    }

    for.plot <- list(species.rep, between.species)
    boxplot(for.plot, ylab=paste0(method, " correlation"), names=c("Within\nspecies", "Between\nspecies"))
}

pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_reproducibility_by_levels_clean.pdf"))
plot.reproducibility(rpkmNormLoess$E, samplesMeta, "spearman")
plot.reproducibility(rpkmNormLoess$E, samplesMeta, "pearson")
plot.reproducibility(rpkmNormNone$E, samplesMeta, "spearman")
plot.reproducibility(rpkmNormNone$E, samplesMeta, "pearson")
plot.reproducibility(rpkmNorm$E, samplesMeta, "spearman")
plot.reproducibility(rpkmNorm$E, samplesMeta, "pearson")
dev.off()

###########################################
### 4. DIFFERENTIAL EXPRESSION TESTING. ###
###########################################

## Loess norm with individual blocking/random effect
rpkmNormLoessFit <- lmFit(rpkmNormLoess, design)
rpkmNormNoneFit <- lmFit(rpkmNormNone, design)
rpkmNormFit <- lmFit(rpkmNorm, design)

myargs <- list(paste0("human-", extraVars[1]), levels=design)
contrasts <- do.call(makeContrasts, myargs)

rpkmNormLoessFit2 <- contrasts.fit(rpkmNormLoessFit, contrasts)
rpkmNormLoessFit2 <- eBayes(rpkmNormLoessFit2)

topGenesLoess <- topTable(rpkmNormLoessFit2, adjust="BH", number=Inf, sort.by="p")
print("Number of DE exons at FDR 1%")
print(table(decideTests(rpkmNormLoessFit2, adjust.method="BH", method="separate", p.value=0.01)))

print("Number of DE exons at FDR 5%")
print(table(decideTests(rpkmNormLoessFit2, adjust.method="BH", method="separate", p.value=0.05)))

print("Number of DE exons at FDR 10%")
print(table(decideTests(rpkmNormLoessFit2, adjust.method="BH", method="separate", p.value=0.10)))

write.table(topGenesLoess, file=paste0(extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_voom.loess.cpm.polyester_testing_results.out"), quote=F, row.names=F, col.names=T, sep="\t", eol="\n")

rpkmNormNoneFit2 <- contrasts.fit(rpkmNormNoneFit, contrasts)
rpkmNormNoneFit2 <- eBayes(rpkmNormNoneFit2)

topGenesNone <- topTable(rpkmNormNoneFit2, adjust="BH", number=Inf, sort.by="p")
print("Number of DE exons at FDR 1%")
print(table(decideTests(rpkmNormNoneFit2, adjust.method="BH", method="separate", p.value=0.01)))

print("Number of DE exons at FDR 5%")
print(table(decideTests(rpkmNormNoneFit2, adjust.method="BH", method="separate", p.value=0.05)))

print("Number of DE exons at FDR 10%")
print(table(decideTests(rpkmNormNoneFit2, adjust.method="BH", method="separate", p.value=0.10)))

write.table(topGenesLoess, file=paste0(extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_voom.none.cpm.polyester_testing_results.out"), quote=F, row.names=F, col.names=T, sep="\t", eol="\n")

rpkmNormFit2 <- contrasts.fit(rpkmNormFit, contrasts)
rpkmNormFit2 <- eBayes(rpkmNormFit2)

topGenes <- topTable(rpkmNormFit2, adjust="BH", number=Inf, sort.by="p")
print("Number of DE exons at FDR 1%")
print(table(decideTests(rpkmNormFit2, adjust.method="BH", method="separate", p.value=0.01)))

print("Number of DE exons at FDR 5%")
print(table(decideTests(rpkmNormFit2, adjust.method="BH", method="separate", p.value=0.05)))

print("Number of DE exons at FDR 10%")
print(table(decideTests(rpkmNormFit2, adjust.method="BH", method="separate", p.value=0.10)))

write.table(topGenes, file=paste0(extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_voom.none_no_filter.cpm.polyester_testing_results.out"), quote=F, row.names=F, col.names=T, sep="\t", eol="\n")


###







allMethods <- merge(topGenesLoess, topGenesNone, by.x="genes", by.y="genes", suffix=c(".Loess", ".Filter"))
allMethods <- merge(allMethods, topGenes, by.x="genes", by.y="genes")

### Venn diagram function:
make.venn.triple <- function(geneset1, geneset2, geneset3, geneset1.label, geneset2.label, geneset3.label, universe){
    universe$g1 <- universe$genes %in% geneset1
    universe$g2 <- universe$genes %in% geneset2
    universe$g3 <- universe$genes %in% geneset3
    venn.placeholder <- draw.triple.venn(length(geneset1), length(geneset2), length(geneset3), dim(universe[universe$g1 == T & universe$g2 == T,])[1], dim(universe[universe$g2 == T & universe$g3 == T,])[1], dim(universe[universe$g1 == T & universe$g3 == T,])[1], dim(universe[universe$g1 == T & universe$g2 == T & universe$g3 == T,])[1], c(geneset1.label, geneset2.label, geneset3.label), fill=c("goldenrod", "plum4", "steelblue3"), alpha=c(0.5, 0.5, 0.5),col=NA, euler.d=T)
    complement.size <- dim(universe[universe$g1 == F & universe$g2 == F & universe$g3 == F,][1])
    grid.text(paste(complement.size, " not DE\nin any", sep=""), x=0.1, y=0.1)
    print(paste("Genes in a: ", length(geneset1), sep=""))
    print(paste("Genes in b: ", length(geneset2), sep=""))
    print(paste("Genes in c: ", length(geneset3), sep=""))
    print(paste("Common genes: ", dim(universe[universe$g1 == T & universe$g2 == T & universe$g3 == T,])[1], sep=""))
}

pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_DE_overlaps_results.pdf"), height=7, width=7)
    make.venn.triple(allMethods[allMethods$adj.P.Val.Loess < 0.01,]$genes, allMethods[allMethods$adj.P.Val.Filter < 0.01,]$genes, allMethods[allMethods$adj.P.Val < 0.01,]$genes, "DE loess,\nfilt", "DE no norm,\nfilt", "DE no norm,\nno filt", allMethods)
    grid.newpage()
    make.venn.triple(allMethods[allMethods$adj.P.Val.Loess < 0.05,]$genes, allMethods[allMethods$adj.P.Val.Filter < 0.05,]$genes, allMethods[allMethods$adj.P.Val < 0.05,]$genes, "DE loess,\nfilt", "DE no norm,\nfilt", "DE no norm,\nno filt", allMethods)
    grid.newpage()
    make.venn.triple(allMethods[allMethods$adj.P.Val.Loess < 0.10,]$genes, allMethods[allMethods$adj.P.Val.Filter < 0.10,]$genes, allMethods[allMethods$adj.P.Val < 0.10,]$genes, "DE loess,\nfilt", "DE no norm,\nfilt", "DE no norm,\nno filt", allMethods)
dev.off()


pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_logFC_plots.pdf"))
ggplot() + 
   geom_density(data=allMethods, aes(x=logFC.Loess, fill="1")) + 
   geom_density(data=allMethods, aes(x=logFC.Filter, fill="2")) + 
   geom_density(data=allMethods, aes(x=logFC, fill="3")) + 
   labs(x="log2FC") + 
   scale_fill_manual(name = "", labels = c("Loess, filt", "None, filt", "None, no filt"), values = c(alpha("goldenrod", 0.3), alpha("plum4", 0.3), alpha("steelblue3", 0.3))) +
   guides(fill = guide_legend(override.aes = list(alpha = 0.1)) + 
   theme(legend.position="bottom"))
dev.off()

### QQ plots:
    oLoess = -log10(sort(topGenesLoess$adj.P.Val,decreasing=F))
    oNone = -log10(sort(topGenesNone$adj.P.Val, decreasing=F))
    e = -log10( 1:length(oNone)/length(oNone) )

    ggplot() +
    geom_point(aes(x=e, y=oLoess, colour="1")) +
    geom_point(aes(x=e, y=oNone, colour="2")) +
    geom_abline(slope=1, intercept=0) +
    scale_colour_manual(name = "", labels = c("Loess, filt", "None, filt", "None, no filt"), values = c(alpha("goldenrod", 0.3), alpha("steelblue3", 0.3))) +
    labs(x="expected p (-log10)", y="observed p (-log10)") + 
    theme_bw()


summaryStats <- list()

for (i in c("loess", "none", "none_no_filter")){
    for (j in seq(0.95, 0.99, 0.01)){
        for (k in c(50, 100)){
            for (l in c(10, 20, 100)){
                topGenes <- read.table(file=paste0("ortho_FALSE_", j, "_", k, "bp_", l, "x_voom.", i, ".cpm.polyester_testing_results.out"), stringsAsFactors=F)
                de01 <- dim(topGenes[topGenes$adj.P.Val <= 0.01,])[1]
                de05 <- dim(topGenes[topGenes$adj.P.Val <= 0.05,])[1]
                de10 <- dim(topGenes[topGenes$adj.P.Val <= 0.10,])[1]
                allGenes <- dim(topGenes)[1]
                rowStats <- c(j,k,l,i,allGenes,de01,de05,de10)
                summaryStats <- c(summaryStats,list(rowStats))
            }
        }
    }
}


ortho_FALSE_0.99_50bp_10x_voom.loess.cpm.polyester_testing_results.out 

                for m in panTro5; do

                echo "module load R; Rscript --vanilla ~/repos/orthoexon/simulation_de_testing.r ${m} ${i} ${j} ${k} ${l}" | qsub -l walltime=1:00:00 -l ncpus=1 -V -o ~/orthoexon/simNGS/hg38v86_${m}/logs/poly_de_testing_${i}_${j}_${k}_${l}.log -N simDE${i}.${j}.${k}.${l} 

                  done
            done
        done
    done
# done





# And then collapse to gene level and repeat
allMethods$Geneid <- sapply(strsplit(as.character(allMethods$genes), "_"), "[[", 1)

# Define the aggregation function:
checkExonDE <- function(x, threshold){
    LoessDE <- length(which(x$adj.P.Val.Loess <= threshold))
    FilterDE <- length(which(x$adj.P.Val.Filter <= threshold))
    NoneDE <- length(which(x$adj.P.Val <= threshold))
    allStats <- c(LoessDE, FilterDE, NoneDE)
    return(allStats)
}

allGenesDESum01 <- ddply(allMethods, .(allMethods$Geneid), function(x) checkExonDE(x, 0.01))
allGenesDESum05 <- ddply(allMethods, .(allMethods$Geneid), function(x) checkExonDE(x, 0.05))
allGenesDESum10 <- ddply(allMethods, .(allMethods$Geneid), function(x) checkExonDE(x, 0.10))

print("Genes with at least one DE Exon")
print(table(allGenesDESum10$V1 >= 1))

table(allGenesDESum10$V2 >= 1)

table(allGenesDESum10$V3 >= 1)
