#! /bin/bash
# Modified from a script included with XSAnno, Zhu et al, BMC Genomics 2014

baseDir="/Users/irene/Data/orthoexon/simNGS/hg38_macFas5";
outDirBase="/Users/irene/Data/orthoexon/simNGS/hg38_macFas5/simReads"
sp1="hg38"
sp2="macFas5"

for i in {1..10}; do
    for j in 0.9 0.91 0.92 0.93 0.94 0.95; do
        for k in TRUE FALSE; do
            mkdir -p $outDirBase/${j}/${k}
            # The comands, explained:
            # simLibrary:
                # -r 50: read length. Going with the worst case scenario of 50bp, unstranded. 
                # -i 100: mean insert siz. "The mean length of the reads sampled is the insert length plus twice the read length." 
                # -x 10: depth of coverage across the entire transcriptome, set to 10 by Zhu. Might change it later, but 10 is nice and shallow - would be worthwhile considering 15 or 20; let's see how many simulated reads I get with 10 from these transcriptomes. 
                # -p : do not generate paired end reads. 
                # -seed: set seed for reproducibility 

            #simNGS
                # -p single: single end reads. 
                # -o fastq: output type. 
                # -s : seed
                # runfile: a runfile of Illumina intensities/? (not very well documented), downloaded from the simNGS website. Unclear if they are 101 bp long only. https://www.ebi.ac.uk/goldman-srv/simNGS/runfiles5/HiSeq/

            # Simulate humans:
            seed=$(($i*11051984)) # Stupid mac.
            simLibrary -r 50 -i 100 -x 10 --seed ${seed} -p ${baseDir}/fasta/${sp1}_ensembl84_orthoexon_${j}pc_ortho_${k}.fa| simNGS -p single -s ${seed} -o "fastq" /Users/irene/Data/orthoexon/simNGS/runfiles/s_4_4x.runfile > ${outDirBase}/${j}/${k}/sim_${sp1}_${i}_${j}pc_ortho_${k}.fastq

            # Simulate macaques:
            simLibrary -r 50 -i 100 -x 10 --seed ${seed} -p ${baseDir}/fasta/${sp2}_ensembl84_orthoexon_${j}pc_ortho_${k}.fa| simNGS -p single -s ${seed} -o "fastq" /Users/irene/Data/orthoexon/simNGS/runfiles/s_4_4x.runfile > ${outDirBase}/${j}/${k}/sim_${sp2}_${i}_${j}pc_ortho_${k}.fastq

        done
    done
done





