


allMethods <- merge(topGenesLoess, topGenesNone, by.x="genes", by.y="genes", suffix=c(".Loess", ".Filter"))
allMethods <- merge(allMethods, topGenes, by.x="genes", by.y="genes")

### Venn diagram function:
make.venn.triple <- function(geneset1, geneset2, geneset3, geneset1.label, geneset2.label, geneset3.label, universe){
    universe$g1 <- universe$genes %in% geneset1
    universe$g2 <- universe$genes %in% geneset2
    universe$g3 <- universe$genes %in% geneset3
    venn.placeholder <- draw.triple.venn(length(geneset1), length(geneset2), length(geneset3), dim(universe[universe$g1 == T & universe$g2 == T,])[1], dim(universe[universe$g2 == T & universe$g3 == T,])[1], dim(universe[universe$g1 == T & universe$g3 == T,])[1], dim(universe[universe$g1 == T & universe$g2 == T & universe$g3 == T,])[1], c(geneset1.label, geneset2.label, geneset3.label), fill=c("goldenrod", "plum4", "steelblue3"), alpha=c(0.5, 0.5, 0.5),col=NA, euler.d=T)
    complement.size <- dim(universe[universe$g1 == F & universe$g2 == F & universe$g3 == F,][1])
    grid.text(paste(complement.size, " not DE\nin any", sep=""), x=0.1, y=0.1)
    print(paste("Genes in a: ", length(geneset1), sep=""))
    print(paste("Genes in b: ", length(geneset2), sep=""))
    print(paste("Genes in c: ", length(geneset3), sep=""))
    print(paste("Common genes: ", dim(universe[universe$g1 == T & universe$g2 == T & universe$g3 == T,])[1], sep=""))
}

pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_DE_overlaps_results.pdf"), height=7, width=7)
    make.venn.triple(allMethods[allMethods$adj.P.Val.Loess < 0.01,]$genes, allMethods[allMethods$adj.P.Val.Filter < 0.01,]$genes, allMethods[allMethods$adj.P.Val < 0.01,]$genes, "DE loess,\nfilt", "DE no norm,\nfilt", "DE no norm,\nno filt", allMethods)
    grid.newpage()
    make.venn.triple(allMethods[allMethods$adj.P.Val.Loess < 0.05,]$genes, allMethods[allMethods$adj.P.Val.Filter < 0.05,]$genes, allMethods[allMethods$adj.P.Val < 0.05,]$genes, "DE loess,\nfilt", "DE no norm,\nfilt", "DE no norm,\nno filt", allMethods)
    grid.newpage()
    make.venn.triple(allMethods[allMethods$adj.P.Val.Loess < 0.10,]$genes, allMethods[allMethods$adj.P.Val.Filter < 0.10,]$genes, allMethods[allMethods$adj.P.Val < 0.10,]$genes, "DE loess,\nfilt", "DE no norm,\nfilt", "DE no norm,\nno filt", allMethods)
dev.off()


pdf(file=paste0("eda_plots/", extraVars[2], "_", extraVars[3], "_", extraVars[4], "bp_", extraVars[5], "x_logFC_plots.pdf"))
ggplot() + 
   geom_density(data=allMethods, aes(x=logFC.Loess, fill="1")) + 
   geom_density(data=allMethods, aes(x=logFC.Filter, fill="2")) + 
   geom_density(data=allMethods, aes(x=logFC, fill="3")) + 
   labs(x="log2FC") + 
   scale_fill_manual(name = "", labels = c("Loess, filt", "None, filt", "None, no filt"), values = c(alpha("goldenrod", 0.3), alpha("plum4", 0.3), alpha("steelblue3", 0.3))) +
   guides(fill = guide_legend(override.aes = list(alpha = 0.1)) + 
   theme(legend.position="bottom"))
dev.off()

### QQ plots:
    oLoess = -log10(sort(topGenesLoess$adj.P.Val,decreasing=F))
    oNone = -log10(sort(topGenesNone$adj.P.Val, decreasing=F))
    e = -log10( 1:length(oNone)/length(oNone) )

    ggplot() +
    geom_point(aes(x=e, y=oLoess, colour="1")) +
    geom_point(aes(x=e, y=oNone, colour="2")) +
    geom_abline(slope=1, intercept=0) +
    scale_colour_manual(name = "", labels = c("Loess, filt", "None, filt", "None, no filt"), values = c(alpha("goldenrod", 0.3), alpha("steelblue3", 0.3))) +
    labs(x="expected p (-log10)", y="observed p (-log10)") + 
    theme_bw()


summaryStats <- list()

for (i in c("loess", "none", "none_no_filter")){
    for (j in seq(0.95, 0.99, 0.01)){
        for (k in c(50, 100)){
            for (l in c(10, 20, 100)){
                topGenes <- read.table(file=paste0("ortho_FALSE_", j, "_", k, "bp_", l, "x_voom.", i, ".cpm.polyester_testing_results.out"), stringsAsFactors=F)
                de01 <- dim(topGenes[topGenes$adj.P.Val <= 0.01,])[1]
                de05 <- dim(topGenes[topGenes$adj.P.Val <= 0.05,])[1]
                de10 <- dim(topGenes[topGenes$adj.P.Val <= 0.10,])[1]
                allGenes <- dim(topGenes)[1]
                rowStats <- c(j,k,l,i,allGenes,de01,de05,de10)
                summaryStats <- c(summaryStats,list(rowStats))
            }
        }
    }
}


ortho_FALSE_0.99_50bp_10x_voom.loess.cpm.polyester_testing_results.out 

                for m in panTro5; do

                echo "module load R; Rscript --vanilla ~/repos/orthoexon/simulation_de_testing.r ${m} ${i} ${j} ${k} ${l}" | qsub -l walltime=1:00:00 -l ncpus=1 -V -o ~/orthoexon/simNGS/hg38v86_${m}/logs/poly_de_testing_${i}_${j}_${k}_${l}.log -N simDE${i}.${j}.${k}.${l} 

                  done
            done
        done
    done
# done





# And then collapse to gene level and repeat
allMethods$Geneid <- sapply(strsplit(as.character(allMethods$genes), "_"), "[[", 1)

# Define the aggregation function:
checkExonDE <- function(x, threshold){
    LoessDE <- length(which(x$adj.P.Val.Loess <= threshold))
    FilterDE <- length(which(x$adj.P.Val.Filter <= threshold))
    NoneDE <- length(which(x$adj.P.Val <= threshold))
    allStats <- c(LoessDE, FilterDE, NoneDE)
    return(allStats)
}

allGenesDESum01 <- ddply(allMethods, .(allMethods$Geneid), function(x) checkExonDE(x, 0.01))
allGenesDESum05 <- ddply(allMethods, .(allMethods$Geneid), function(x) checkExonDE(x, 0.05))
allGenesDESum10 <- ddply(allMethods, .(allMethods$Geneid), function(x) checkExonDE(x, 0.10))

print("Genes with at least one DE Exon")
print(table(allGenesDESum10$V1 >= 1))

table(allGenesDESum10$V2 >= 1)

table(allGenesDESum10$V3 >= 1)
