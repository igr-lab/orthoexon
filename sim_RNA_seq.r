### sim_RNA_seq.r
### Generates simulated RNA-seq reads for different orthoExon fasta files (see blat_processing_intraspecies_bed12.R and notebook posts on 16.09.22 for context etc)
### IGR 16.09.23

### 0. PREPARE WORKSPACE. ###
### 1. READ IN FILES AND SIMULATE. ### 

### Slow, should be run as a command-line executable and preferably submitted as a batch job. 
### Requires 4 options:
### 1 to many orthology filtering: "ortho_TRUE" or "ortho_FALSE"
### percent identity threshold: 0.9, 0.91, 0.92, 0.93, 0.94, 0.95
### read length: 50 or 100, mostly. 
### second species: macFas5 or panTro5, for now. Paths will have to be altered if things get more creative.

### command line example: 
### Rscript --vanilla ~/repos/orthoexon/sim_RNA_seq.r ortho_TRUE 0.9 100 macFas5

### Last updated: 17.04.04: added command line argument for second species, instead of hard coding it. Paths to references etc are still prety hardcoded, though. 

#############################
### 0. PREPARE WORKSPACE. ###
#############################

extraVars <- commandArgs(trailingOnly=T)
#extraVars <- c("ortho_FALSE", 0.99, 50, "panTro5", 10) # for testing only

library(polyester)
library(Biostrings)

setwd(paste0("/scratch/SG0008/orthoexon/polyester/hg38v86_", extraVars[4]))

######################################
### 1. READ IN FILES AND SIMULATE. ### 
######################################

# FASTA annotation
print("Reading fasta files...")
hsFastaPath <- paste0(getwd(), "/fasta/hg38_ensembl86_orthoexon_", extraVars[2], "pc_", extraVars[1], ".fa")
secondSpeciesFastaPath <- paste0(getwd(), "/fasta/", extraVars[4], "_ensembl86_orthoexon_", extraVars[2], "pc_", extraVars[1], ".fa")
hsFasta <- readDNAStringSet(hsFastaPath)
secondSpeciesFasta <- readDNAStringSet(secondSpeciesFastaPath)

# ~20x coverage ----> reads per transcript = transcriptlength/readlength * 20
# here all transcripts will have ~equal FPKM
hsDepth <- round(as.numeric(extraVars[5]) * width(hsFasta) / 100)
secondSpeciesDepth <- round(as.numeric(extraVars[5]) * width(secondSpeciesFasta) / 100)

# Simulate humans:
print("Simulating RNA seq data for humans...")

# Check if simulation has been run already or not...
Houtdir <- paste0(getwd(), "/polyester/", extraVars[1], "_", extraVars[2], "_", extraVars[3], "bp_", extraVars[5], "x/hg38")

if (!file.exists(paste0(Houtdir, "/sample_10.fasta.gz"))) {
    dir.create(file.path(Houtdir), showWarnings = F, recursive=T)
    simulate_experiment(fasta = hsFastaPath, outdir = Houtdir, num_reps = c(5, 5), reads_per_transcript = hsDepth, size=NULL, fold_changes=matrix(1, ncol=2, nrow=length(hsFasta)), paired = F, reportCoverage = FALSE, readlen=as.numeric(extraVars[3]), seed=round(584*as.numeric(extraVars[2])), gzip=TRUE)
} else {
    print("Human data already simulated, skipping.")
}

# Simulate second species:
print(paste0("Simulating RNA seq data for ", extraVars[4], "..."))

# Again, check for completeness...
SSoutdir <- paste0(getwd(), "/polyester/", extraVars[1], "_", extraVars[2], "_", extraVars[3], "bp_", extraVars[5], "x/", extraVars[4])

if (!file.exists(paste0(SSoutdir, "/sample_10.fasta.gz"))){
    dir.create(file.path(SSoutdir), showWarnings = F, recursive=T)
    simulate_experiment(fasta = secondSpeciesFastaPath, outdir = SSoutdir, num_reps = c(5, 5), reads_per_transcript = secondSpeciesDepth, size = NULL, fold_changes=matrix(1, ncol=2, nrow=length(secondSpeciesFasta)), paired = F, reportCoverage = FALSE, readlen=as.numeric(extraVars[3]), seed=round(485*as.numeric(extraVars[2])), gzip=TRUE)
} else {
    print(paste0(extraVars[4], " data already simulated, skipping."))
}